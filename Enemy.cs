using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Квест
{
    public class Enemy
    {
        public int hp;
        Random rnd = new Random();
        public Enemy()
        {
            this.hp = 100;
        }

        public int damage()
        {
            return rnd.Next(20, 32);
        }
    }
}
