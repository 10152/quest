using Квест;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

int res = 2;
// цель: дойти до деревни 
// проклятый остров 
Console.WriteLine($"Здравствуй странник, ты только что очнулся. Ох, ну тебя и потрепало!");
Console.WriteLine("Как звать тебя?");

string name = Console.ReadLine();
Person prsn = new Person(name);

Console.WriteLine($"Здравствуй, {name}, ты потерпел крушение... ");
Console.WriteLine("Таким как ты на острове есть одно место...");
Console.WriteLine("Деревня отшелников, отправляйся туда.");


void gamestart()
{
    Console.WriteLine("1 - играть");
    Console.WriteLine("2 - выйти");
    int x = Convert.ToInt32(Console.ReadLine());
    if (x == 1)
    {
        Console.Clear();
        Console.WriteLine("Вперед");
        Console.WriteLine();
        Console.Clear();
        lvl1();
    }
    else if(x == 2)
    {
        Console.WriteLine("Прощай");
    }
    else
    {
        while (true)
        {
            if (x != 1 || x != 2)
            {
                Console.WriteLine("Введите корректное число");
                gamestart();
            }
        }
    }
}

void lvl1()
{
    Console.WriteLine("Взяв с собой меч ри доспехи ты отправился на поиск деревни.");
    Console.WriteLine("Перед тобой стоит выбор пойти в пещеру или пройти мимо.");
    Console.WriteLine("1 - Пройти мимо");
    Console.WriteLine("2 - Пойти в пещеру");
    int i = Convert.ToInt32(Console.ReadLine());
    if (i == 2)
    {
        Console.Clear();
        Console.WriteLine("Зайдя в пищеру и проядя ее, ты оказываешся на берегу рядом с пальмой");
        Console.WriteLine("Что делаешь?");
        lvl2();
    }
    else if (i == 1)
    {
        Console.Clear();
        Console.WriteLine("Пройдя пещеру, на тебя нападает амогус и ты умираешь");
        Console.ForegroundColor = ConsoleColor.Red;
        Console.WriteLine("Поражение");
        Console.ResetColor();
        Console.ReadLine();
        Console.Clear();
        gamestart();
    }
    else
    {
        Console.Clear();
        while (true)
        {
            if (i != 1 || i != 2)
            {
                Console.WriteLine("введите число из правильного диапазона");
                lvl1();
            }
        }
    }
}

void lvl2()
{
    Console.WriteLine("1 - Спилить");
    Console.WriteLine("2 - Обойти");

    int i = Convert.ToInt32(Console.ReadLine());
    if (i == 1)
    {
        Console.WriteLine("Опа.. А пилы то у тебя нет. Но ты её можем получить у торговца, обменяв на твои доспехи и меч");
        Console.WriteLine("Обменять?");
        Console.WriteLine("1 - да");
        Console.WriteLine("2 - нет");
        int u = Convert.ToInt32(Console.ReadLine());
        if (u == 1)
        {
            Console.WriteLine("Торговец оказался таким щедрым, что дал не только пилу, но и кучу других инструментов.");
            Console.WriteLine("Поэтому мы из пальмы делаем лодку и уплываем, но на нас нападает Аводус");
            Console.WriteLine("Без оружия ты не смог защитить себя...");
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Поражение");
            Console.ResetColor();
            Console.ReadLine();
            Console.Clear();
            gamestart();
            
        }
        else if (u == 2)
        {
            Console.Clear();
            lvl2();
        }
    }
    
    
    else if (i == 2)
    {
        Console.WriteLine("Тебе на пути попадается ужасное существо - Абоба");
        Console.WriteLine("Сражайся за свою жизнь...");
        Console.ReadLine();
        Console.Clear();

        fight();
    }
}

void lvl3()
{
    Console.WriteLine("Пройдя дальше в сторону джунглей преед тобой встал выбор...");
    Console.WriteLine("Пройти через джунгли или проёти мимо озера");
    Console.WriteLine("1 - Джунгли");
    Console.WriteLine("2 - Озеро"); //джунгли -> деревня; озеро -> драка -> каньён (мост(смерть) или обойти(деревня))

    int i = Convert.ToInt32(Console.ReadLine());
    if(i == 1)
    {
        Console.WriteLine("Ты выбрал джунгли");
        Console.WriteLine("Чудом ты оказля жив");
        Console.WriteLine("Смотри! Ты дошёл до деревни!");
        Console.ForegroundColor = ConsoleColor.Green;
        Console.WriteLine("Победа");
        Console.ResetColor();
    }
    else
    {
        Console.WriteLine("На тебя нападает Ивпатий, разбойник.");
        Console.WriteLine("Срази его ведь победа близка");
        fight2();
    }
}
void lvl4()
{
    Console.WriteLine("После победного танца ты замечаешь каньён");
    Console.WriteLine("Пройдёшь по мосту через него или найдёшь обход?");
    Console.WriteLine("1 - Мост");
    Console.WriteLine("2 - Обход");

    int i = Convert.ToInt32(Console.ReadLine());
    if (i == 1)
    {
        Console.WriteLine("Мост оказался слишком хлипким и ты упал в пропасть...");
        Console.WriteLine("Ты проиграл");
        Console.ForegroundColor = ConsoleColor.Red;
        Console.WriteLine("Поражение");
        Console.ResetColor();
        gamestart();
    }
    else
    {
        Console.WriteLine("Поздравляю, ты нашел деревню!");
        Console.WriteLine("Ты выиграл");
        Console.ForegroundColor = ConsoleColor.Green;
        Console.WriteLine("Победа");
        Console.ResetColor();
    }
}

Random rnd = new Random();

void fight()
{
    Enemy aboba = new Enemy();
    prsn.hp -= 16;
    Console.WriteLine($"На тебя напала абоба и отняла 20 xп");
    Console.WriteLine("Для того, чтобы ударить абобу нажмите нажмите enter");
    while (prsn.hp > 0 || aboba.hp > 0)
    {
        Console.ReadLine();
        aboba.hp -= prsn.dmg;
        Console.WriteLine("Твой удар " + prsn.dmg);
        if (aboba.hp <= 0)
        {
            res = 1;
            Console.WriteLine("Здоровье абобы - 0");
            break;
        }
        Console.WriteLine("Здоровье абобы " + aboba.hp);
        Console.WriteLine("---------------");
        Console.WriteLine("Удар абобы " + aboba.damage());
        prsn.hp -= aboba.damage();
        if (prsn.hp <= 0)
        {
            res = 0;
            Console.WriteLine("Твоё здоровьё - 0");
            break;
        }
        Console.WriteLine("Твоё здоровье " + prsn.hp);
        Console.WriteLine("---------------");
    }
    if (res == 1)
    {
        Console.WriteLine("Ты сразил Абобу");
        Console.ForegroundColor = ConsoleColor.Green;
        Console.WriteLine("Продолжай двигатся дальше");
        Console.ResetColor();
        prsn.hp += 50;
        lvl3();
    }
    else if (res == 0)
    {
        Console.WriteLine("Ты проиграл");
        Console.ForegroundColor = ConsoleColor.Red;
        Console.WriteLine("Поражение");
        Console.ResetColor();
    }
    Console.ReadLine();
    gamestart();
    
}
void fight2()
{
    Enemy aboba = new Enemy();
    prsn.hp = 100;
    prsn.hp -= 10;
    Console.WriteLine($"Ивпатий отнял 10 xп");
    Console.WriteLine("Для того, чтобы ударить Ивпатия нажмите нажмите enter");
    while (prsn.hp > 0 || aboba.hp > 0)
    {
        Console.ReadLine();
        aboba.hp -= prsn.dmg;
        Console.WriteLine("Твой удар " + prsn.dmg);
        if (aboba.hp <= 0)
        {
            res = 1;
            Console.WriteLine("Здоровье Ивпатия - 0");
            break;
        }
        Console.WriteLine("Здоровье Ивпатия " + aboba.hp);
        Console.WriteLine("---------------");
        Console.WriteLine("Удар Ивпатия " + aboba.damage());
        prsn.hp -= aboba.damage();
        if (prsn.hp <= 0)
        {
            res = 0;
            Console.WriteLine("Твоё здоровьё - 0");
            break;
        }
        Console.WriteLine("Твоё здоровье " + prsn.hp);
        Console.WriteLine("---------------");
    }
    if (res == 1)
    {
        Console.WriteLine("Ты выиграл");
        Console.ForegroundColor = ConsoleColor.Green;
        Console.WriteLine("Победа");
        Console.ResetColor();
        prsn.hp += 50;
        lvl4();
    }
    else if (res == 0)
    {
        Console.WriteLine("Ты проиграл");
        Console.ForegroundColor = ConsoleColor.Red;
        Console.WriteLine("Поражение");
        Console.ResetColor();
    }
    Console.ReadLine();
    gamestart();

}

gamestart();
